<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id_user
 * @property string $login
 * @property string $pass
 * @property string $script
 * @property string $money
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, pass', 'length', 'max'=>255),
            array('money', 'length', 'max'=>20),
            array('script', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_user, login, pass, script, money', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_user' => 'Id User',
            'login' => 'Login',
            'pass' => 'Pass',
            'script' => 'Script',
            'money' => 'Money',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_user',$this->id_user);
        $criteria->compare('login',$this->login,true);
        $criteria->compare('pass',$this->pass,true);
        $criteria->compare('script',$this->script,true);
        $criteria->compare('money',$this->money,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    //Save encrypted password
    public static function hash($value){
        return crypt($value, 'eco');
    }

    //calling hash to encrypt given password
    /*protected function beforeSave(){
        if(parent::beforeSave()){
            $this->pass = $this->hash($this->pass);
            return true;
        }
        return false;
    }*/

    //check if the password is matched with stored encrypted password
    public function validatePassword($value){
        //$new_hash = crypt($value, $this->pass);
        $new_hash = self::hash($value);
        if($new_hash == $this->pass){
            return true;
        }
        return false;
    }

    public static function get($id_user = null) {
        return self::model()->findByPk($id_user? $id_user : Yii::app()->user->id);
    }
}
