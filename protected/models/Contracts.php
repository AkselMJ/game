<?php

/**
 * This is the model class for table "contracts".
 *
 * The followings are the available columns in table 'contracts':
 * @property integer $id_contract
 * @property integer $type
 * @property integer $id_user
 * @property integer $id_recipe
 * @property integer $stages
 * @property integer $stage
 * @property integer $count
 * @property string $price_accept
 * @property string $price
 * @property integer $life
 */
class Contracts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contracts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('stages', 'required'),
			array('type, id_user, id_recipe, stages, stage, count, life', 'numerical', 'integerOnly'=>true),
			array('price_accept, price', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_contract, type, id_user, id_recipe, stages, stage, count, price_accept, price, life', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'recipe' => array(self::HAS_ONE, 'ProductionRecipe', ['id_recipe' => 'id_recipe'])
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contract' => 'Id Contract',
			'type' => 'Type',
			'id_user' => 'Id User',
			'id_recipe' => 'Id Recipe',
			'stages' => 'Stages',
			'stage' => 'Stage',
			'count' => 'Count',
			'price_accept' => 'Price Accept',
			'price' => 'Price',
			'life' => 'Life',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contract',$this->id_contract);
		$criteria->compare('type',$this->type);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_recipe',$this->id_recipe);
		$criteria->compare('stages',$this->stages);
		$criteria->compare('stage',$this->stage);
		$criteria->compare('count',$this->count);
		$criteria->compare('price_accept',$this->price_accept,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('life',$this->life);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contracts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function newOrder() {
        foreach (ProductionRecipe::model()->findAll("type = 1") as $recipe) {
            $id_item = $recipe->output[0]->id_item;
            $item = Items::model()->find("id_item = $id_item");
            $contract = new self();
            $contract->setAttributes([
                'type' => 1,
                'id_recipe' => $recipe->id_recipe,
                'stages' => rand(3, 10),
                'count' => rand(150, 300),
                'price' => rand($item->base_price * 20, $item->base_price * 150) / 100,
                'price_accept' => 100,
                'life' => rand(20, 80)
            ]);
            $contract->save();
        }
    }

    public static function newProduction() {
	    foreach (ProductionRecipe::model()->findAll("type = 2") as $recipe) {
	        $id_item = $recipe->output[0]->id_item;
            $item = Items::model()->find("id_item = $id_item");
            $contract = new self();
            $count = rand(100, 200);
            $contract->setAttributes([
                'type' => 2,
                'id_recipe' => $recipe->id_recipe,
                'stages' => rand(3, 10),
                'count' => $count,
                'price' => ceil($count * $item->base_price / (rand(100, 300) / 100) * (rand(100, 300) / 100)),
                'price_accept' => 100,
                'life' => rand(20, 80)
            ]);
            $contract->save();
        }
    }
}
