<?php

//$config=require(dirname(__FILE__).'/common.php');

$config = array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' =>'',

	'import'=>array(
		'application.models.*',
		'application.forms.*',
		'application.components.*'
	),

	'modules'=> array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'777',
            'ipFilters' => array('*.*.*.*')
        ),
	),

    'aliases' => [
        'PhpParser' => dirname(__FILE__) . '/../extensions/phpparser',
    ],

	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
			'loginUrl'=>array('site/login'),
		),
		/*'db'=>array(
			'class'=>'system.db.CDbConnection',
            'connectionString' => 'mysql:host=db4free.net;port=3307;dbname=economicgamecom',
            'username' => 'economicgamecom',
            'password' => '57241900',
            'charset'=>'utf8',
            // включаем профайлер
            'enableProfiling'=>true,
            // показываем значения параметров
            'enableParamLogging' => true,
		),*/
        'db'=>array(
			'class'=>'system.db.CDbConnection',
            'connectionString' => 'mysql:host=192.168.1.201;port=3306;dbname=economicgamecom',
            'username' => 'root',
            'password' => '',
            'charset'=>'utf8',
            // включаем профайлер
            'enableProfiling'=>true,
            // показываем значения параметров
            'enableParamLogging' => true,
		),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace, info, error, warning, vardump',
                ),
                // uncomment the following to show log messages on web pages
                array(
                    'class' => 'CWebLogRoute',
                    'enabled' => YII_DEBUG,
                    'levels' => 'error, warning, trace, notice',
                    'categories' => 'application',
                    'showInFireBug' => false,
                ),
            ),
        ),

        'clientScript'=> require dirname(__FILE__) . '/clientscript.php'
	),

    /*'log' => array(
        'class' => 'CLogRouter',
        'routes' => array(
            array(
                'class' => 'CFileLogRoute',
                'levels' => 'trace, info, error, warning, vardump',
            ),
            // uncomment the following to show log messages on web pages
            array(
                'class' => 'CWebLogRoute',
                'enabled' => YII_DEBUG,
                'levels' => 'error, warning, trace, notice',
                'categories' => 'application',
                'showInFireBug' => false,
            ),
        ),
    ),*/
);

return $config;