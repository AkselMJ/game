<?php
//$browser = get_browser();

//$includeIE = ($browser->browser='IE' && isset($browser->majorver) && $browser->majorver>8 && $browser->majorver<10);

return array(
		'coreScriptPosition'=>CClientScript::POS_END,
		'packages' => array(
                'jquery'=>array(
                    'basePath'=>'ext.jquery',
                    'js'=>array('jquery.min.js'),
                ),
                'jqueryui'=>array(
                    'basePath'=>'ext.jqueryui',
                    'js'=>array('jquery-ui.min.js'),
                    'css'=>array('jquery-ui.min.css')
                ),
                'bootstrap' => array(
                    'basePath'=>'ext.bootstrap',
                    'js'=>array('bootstrap.min.js'),
                    'css'=>array('bootstrap.min.css'),
                    'depends'=>array('jquery', 'tether'),
                ),
                'tether' => array(
                    'basePath'=>'ext.tether',
                    'js'=>array('tether.min.js')
                ),
                'lobipanel'=>array(
                    'basePath'=>'ext.lobipanel',
                    'js'=>array('lobipanel.js'),
                    'css'=>array('lobipanel.css')
                ),
                'codemirror'=>array(
                    'basePath'=>'ext.codemirror',
                    'js'=>array('codemirror.js', 'php.js', 'htmlmixed.js', 'css.js', 'matchbrackets.js', 'clike.js', 'xml.js', 'javascript.js'),
                    'css'=>array('codemirror.css')
                ),
		)
);
