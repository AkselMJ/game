<?php

class CommonCommand extends CConsoleCommand {
    //use environment;
	public function actionStep()
    {
	    $step = GameValues::model()->find("name = 'step'");
        $step->value++;
        $step->save();

        $city = Cities::model()->find();

        $storages = CityStorage::model()->findAll();
        foreach ($storages as $storage) {
            $new_count = $storage->count - $storage->item->consume * $city->population;
            if ($new_count >= 0) {
                $storage->count = $new_count;
                $storage->save();
                $city->population += ceil($city->population * 0.01);
                $storage->price -= $storage->price * 0.01;
                if ($storage->price * 0.2 >= $storage->item->base_price) {
                    $storage->save();
                }
            } else {
                $city->population -= ceil($city->population * 0.01);
                $storage->price += $storage->price * 0.01;
                if ($storage->price <= $storage->item->base_price * 10) {
                    $storage->save();
                }
            }
            if ($city->population < 100) $city->population = 100;
            $city->save();
        }

        $gs = new GameStats();
        $gs->step = $step->value;
        $gs->name = 'population';
        $gs->value = $city->population;
        $gs->save();

        foreach (range(1, rand(1,5)) as $i) {
            Contracts::newOrder();
            Contracts::newProduction();
        }

        $contracts = \Contracts::model()->findAll("id_user is not null");
        foreach ($contracts as $c) {
            $c->stage++;
            if ($c->stage >= $c->stages) {
                $c->delete();
            } else {
                $c->save();
            }
        }

        $contracts = \Contracts::model()->findAll("id_user is null");
        foreach ($contracts as $c) {
            $c->life--;
            if ($c->life <= 0) {
                $c->delete();
            } else {
                $c->save();
            }
        }
    }

    public function actionScripts()
    {
        Yii::setPathOfAlias('environment', Yii::getPathOfAlias('application') . '/components/environment');
        Yii::setPathOfAlias('phpparser', Yii::getPathOfAlias('application') . '/components/phpparser');
        $users = User::model()->findAll();

        foreach ($users as $user) {
            if (!empty($user->script)) {
                $economic = new \environment\economic($user->id_user);
                $economic->evaluate($user->script);
            }
        }
    }
}