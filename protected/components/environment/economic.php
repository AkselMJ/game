<?php declare(strict_types=1);

namespace environment;
class economic
{
    private $id_user;

    function __construct($id_user)
    {
        $this->id_user = $id_user;
    }

    public function evaluate($script, $env = [])
    {
        $economic = $this;
        $tmpHandle = tmpfile();
        $metaDatas = stream_get_meta_data($tmpHandle);
        $tmpFilename = $metaDatas['uri'];

        fwrite($tmpHandle, "<?php namespace environment; " . $script . " ?>");
        file_get_contents($tmpFilename);
        include $tmpFilename;
        fclose($tmpHandle);
    }

    public static function getMethods()
    {
        $desc = [
            'sell'                  => 'Продаёт указанное количество товара с склада игрока на городской рынок.',
            'getCityContracts'      => 'Получает список контрактов доступных в городе.',
            'getMyContracts'        => 'Получает список контрактов, принадлежащих игроку.',
            'contractOrder'         => 'Выполняет контракт поставок (доставляет товар на склад игрока).',
            'contractProduction'    => 'Забирает со склада необходимое количество предметов для производства и доставляет продукты на склад игрока.',
            'acceptContract'        => 'Переводит контракт в владение игроком.',
            'getMyStorage'          => 'Возвращает информацию о хранилище игрока.'
        ];

        foreach (get_class_methods('environment\economic') as $method) {
            if (!in_array($method, array_keys($desc))) continue;
            $reflection = new \ReflectionMethod('environment\economic', $method);

            $methods[$method]['param'] = [];
            $methods[$method]['desc'] = $desc[$method];
            foreach ($reflection->getParameters() AS $arg) {
                $methods[$method]['param'][$arg->name]['type'] = $arg->getType();
            }
        }

        return $methods;
    }

    private function toStd($array, $index = null) {
        $result = [];

        foreach ($array as $arr) {
            if ($index) {
                $result[$arr->$index] = (object)$arr->attributes;
            } else {
                $result[] = (object)$arr->attributes;
            }
        }

        return $result;
    }

    /*private function makeIndex($c, $index) {
        $result = [];
        foreach ($c as $s) {
            $result[$s->$index] = $s;
        }
        return $result;
    }*/

    private function sell(int $id_item, int $count) {
        $us = \UserStorage::model()->find("id_user = {$this->id_user} and id_item = $id_item");
        $us->count -=$count;
        if ($us->count >= 0) {
            $us->save();

            $user = \User::model()->find("id_user = {$this->id_user}");
            $city_storage = \CityStorage::model()->find("id_item = $id_item");
            $user->money += $count * $city_storage->price;
            $user->save();
            $city_storage->count += $count;
            $city_storage->save();
        }
    }

    private function getCityContracts() {
        $c = \Contracts::model()->findAll("id_user is null");
        return $this->toStd($c, 'id_contract');
    }

    private function getMyContracts() {
        $c = \Contracts::model()->findAll("id_user = {$this->id_user}");
        return $this->toStd($c, 'id_contract');
    }

    private function getMyStorage() {
        $c = \UserStorage::model()->findAll("id_user = {$this->id_user}");

        return $this->toStd($c, 'id_item');
    }

    private function contractOrder(int $id_contract)
    {
        $contract = \Contracts::model()->find("id_contract = $id_contract and type = 1");
        if ($contract == null) return;
        $user = \User::get($this->id_user);
        $user->money -= $contract->price * $contract->count;
        if ($user->money >= 0) {
            $storage = \UserStorage::model()->find("id_user = {$this->id_user} and id_item = {$contract->recipe->output[0]->id_item}");
            $user->save();
            $storage->storage($contract->count);
        }
    }

    private function contractProduction(int $id_contract)
    {
        $user = \User::get($this->id_user);
        $contract = \Contracts::model()->find("id_contract = $id_contract and type = 2");
        if ($contract == null) return;
        $user->money -= $contract->price;
        if ($user->money >= 0) {
            $storage = \UserStorage::model()->find("id_user = {$this->id_user} and id_item = {$contract->recipe->input[0]->id_item}");
            if ($storage != null && $storage->count >= $contract->count) {
                $storage->storage(-1 * $contract->count);
                $storage->storage($contract->count, $this->id_user, $contract->recipe->output[0]->id_item);
                $user->save();
            }
        }
    }

    private function acceptContract(int $id_contract) {
        if (count(\Contracts::model()->findAll("id_user = {$this->id_user}")) < 3) {
            $c = \Contracts::model()->find("id_contract = $id_contract");
            $c->id_user = $this->id_user;
            $c->save();
        }
    }
}