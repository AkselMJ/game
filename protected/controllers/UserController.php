<?php

class UserController extends CController
{
	public function actionIndex()
	{

	}

    public function actionRegister()
    {
        if(isset($_POST['LoginForm'])) {
            $new_user = new User();
            $new_user->login = $_POST['LoginForm']['username'];
            $new_user->pass = User::hash($_POST['LoginForm']['password']);
            $new_user->save();
            $this->redirect(['login']);
        } else {
            $model = new LoginForm;
            $this->render('sign-up', array('model'=>$model));
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if(isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];
            if($model->validate() && $model->login())
                $this->redirect('/');
        }

        $this->render('sign-in',array('model'=>$model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}