<?php
// функции без имени класса
// отправка объектов с количеством в ордера
// security
// accept price

class SiteController extends CController
{

    public function actionIndex()
	{
	    //print_r(Yii::app()->user->id);
	    $storages = CityStorage::model()->findAll();

	    if (Yii::app()->user->isGuest) {
            $user = new User();
            $user_storage = null;
        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $user_storage = UserStorage::model()->findAll("id_user = " . Yii::app()->user->id);
        }

        $city = Cities::model()->find();

        $contracts = [];
        $contracts['orders'] = Contracts::model()->findAll("id_user is null and type = 1");
        $contracts['production'] = Contracts::model()->findAll("id_user is null and type = 2");

        $graph = GameStats::model()->findAll("name = 'population'");

        $r[] = ['Шаг', 'Население'];
        foreach ($graph as $g) {
            $r[] = ["$g->step", (int) $g->value];
        }
        $graph = json_encode($r);

        $this->render('index', compact('storages', 'city', 'user', 'user_storage', 'contracts', 'graph'));
	}

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
            $this->render('error', ['error' => $error]);
    }

    private function checkCode($node, $path)
    {
        $av = [
            'objects' => [
                'PhpParser\Node\Stmt\Expression' => ['node' => 'expr'],
                'PhpParser\Node\Stmt\If_' => [],
                //'PhpParser\Node\Stmt\For_' => [],
                'PhpParser\Node\Stmt\Foreach_' => [],
                'PhpParser\Node\Expr\MethodCall' => ['check' => ['var/name' => ['economic'], 'name/name' => array_keys(\environment\economic::getMethods())], 'type' => ['name' => ['PhpParser\Node\Identifier']]],
                'PhpParser\Node\Expr\Assign' => ['node' => 'expr', 'type' => ['var' => ['PhpParser\Node\Expr\Variable']]]
            ]
        ];

        $class = get_class($node);
        if (!in_array($class, array_keys($av['objects']))) {
            //echo $path . ": " . $class . "\n";
            return false;
        }

        if(isset($av['objects'][$class]['check'])) {
            foreach ($av['objects'][$class]['check'] as $path => $values) {
                $n = $node;
                foreach (explode('/', $path) as $part) {
                    if (is_object($n)) {
                        $n = $n->{$part};
                    } else {
                        $n = $n[$part];
                    }
                }
                if (!in_array($n, $values)) {
                    //echo $path . ": ($n) $class \n";
                    return false;
                }
            }
        }

        if(isset($av['objects'][$class]['type'])) {
            foreach ($av['objects'][$class]['type'] as $field => $values)
            if (!in_array(get_class($node->{$field}), $values)) {
                //echo $path . ": " . $class . "\n";
                return false;
            }
        }

        if(isset($av['objects'][$class]['node'])) {
            $r = $this->checkCode($node->{$av['objects'][$class]['node']}, $path . "/" . $class);
            if ($r === false) {
                return false;
            }
        }

        return true;
    }

    public function actionScript()
    {
        Yii::setPathOfAlias('environment', Yii::getPathOfAlias('application') . '/components/environment');
        $user = User::get();

        $parser = (new \PhpParser\ParserFactory)->create(\PhpParser\ParserFactory::PREFER_PHP7);
        try {
            $ast = $parser->parse("<?php " . $user->script);
        } catch (Error $error) {
            echo "Parse error: {$error->getMessage()}\n";
            return;
        }

        $non_block = true;
        foreach ($ast as $k => $o) {
            if (!$non_block = $this->checkCode($o, $k)) break;
        }

        //print_r(get_class($ast[3]->stmts[0]));
        //echo json_encode($ast);
        //print_r($ast);

        //$dumper = new \PhpParser\NodeDumper;
        //echo $dumper->dump($ast) . "\n";

        $error = false;
        if(isset($_POST['script']))
        {
            $user->script = $_POST['script'];
            if (!preg_match("/eval|select|delete|update|this|parent|file|self|echo|print|\*|\\|\/|\./",$_POST['script']) && $non_block) {
                $user->save();
            } else {
                $error = true;
            }
        }
        // display the login form
        $this->render('script', compact('user', 'error'));
    }

    public function actionApi()
    {
        Yii::setPathOfAlias('environment', Yii::getPathOfAlias('application') . '/components/environment');
        $methods = \environment\economic::getMethods();


        if (Yii::app()->user->isGuest) {
            $user = new User();
        } else {
            $user = User::model()->find("id_user = " . Yii::app()->user->id);
        }

        $this->render('api', compact('user','methods'));
    }

    public function actionItems()
    {
        $items = Items::model()->findAll();

        if (Yii::app()->user->isGuest) {
            $user = new User();
        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
        }

        $this->render('items', compact('items', 'user'));
    }

    public function actionRecipes()
    {
        $recipes = [];
        $recipes[1] = ProductionRecipe::model()->findAll("type = 1");
        $recipes[2] = ProductionRecipe::model()->findAll("type = 2");

        if (Yii::app()->user->isGuest) {
            $user = new User();
        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
        }

        $this->render('recipes', compact('recipes', 'user'));
    }
}