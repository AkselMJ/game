<div class="with-side-menu control-panel control-panel-compact">

    <?=$this->renderPartial("../menu-header", compact('user'), true); ?>
    <?=$this->renderPartial("../menu-side", [], true); ?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 dahsboard-column">
                    <?=$this->renderPartial("./parts/city", compact('city', 'storages'), true); ?>
                </div><!--.col-->
                <div class="col-xl-6 dahsboard-column">
                    <?=$this->renderPartial("../user/parts/storage", compact('user_storage'), true); ?>
                </div><!--.col-->
            </div>
            <div class="row">
                <div class="col-xl-12 dahsboard-column">
                    <section class="widget widget-activity">
                        <div class="widget-header" style="overflow: hidden;">
                            <div class="widget-chart-extra-blue-title">Население</div>
                            <div id="chart_line" class="chart"></div>
                        </div>
                    </section><!--.widget-chart-extra-->
                </div><!--.col-->
            </div>
            <?php /*<div class="row">
                <div class="col-xl-12 dahsboard-column">
                    <section class="box-typical box-typical-dashboard panel panel-default scrollable">
                        <header class="box-typical-header panel-heading">
                            <h3 class="panel-title">Население</h3>
                        </header>
                        <div class="box-typical-body panel-body">
                            <div id="chart_line1" class="chart"></div>
                        </div><!--.box-typical-body-->
                    </section><!--.box-typical-dashboard-->
                </div>
            </div>*/ ?>
            <div class="row">
                <div class="col-xl-6 dahsboard-column">
                    <?=$this->renderPartial("./parts/contract_orders", compact('contracts'), true); ?>
                </div><!--.col-->
                <div class="col-xl-6 dahsboard-column">
                    <?=$this->renderPartial("./parts/contract_production", compact('contracts'), true); ?>
                </div><!--.col-->
            </div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->


<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(document).ready(function() {
    $('.panel').lobiPanel({
        sortable: true,
        reload: false,
        close: false,
        editTitle: false,
        minimize: false,
        unpin: false
    });
    $('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
        $('.dahsboard-column').matchHeight();
    });  
});

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

    var data = google.visualization.arrayToDataTable($graph);
    

    var options = {
        legend: 'none',
        areaOpacity: 0.18,
        tooltip: { trigger: 'none' },
        hAxis: {
            textStyle: {
                color: '#008ffb',
                fontName: 'Proxima Nova',
                fontSize: 11,
                bold: true
            },
            gridlines: 'null'
        },
        vAxis: {
            minValue: 0,
            textStyle: {
                color: '#008ffb',
                fontName: 'Proxima Nova',
                fontSize: 11,
                bold: true
            },
            baselineColor: '#16b4fc',
            ticks: [0,2,4,6,8,10,12],
            gridlines: {
                color: '#1ba0fc',
                count: 7
            },
        },
        lineWidth: 2,
        colors: ['#008ffb'],
        curveType: 'function',
        pointSize: 5,
        pointShapeType: 'circle',
        pointFillColor: '#f00',
        backgroundColor: {
            fill: '#fff',
            strokeWidth: 0
        },
        chartArea:{
            left: '0%',
            top: 5,
            width:'100%',
            height: 200
        }
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_line'));

    chart.draw(data, options);
}
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);
$cs->registerScriptFile('https://www.gstatic.com/charts/loader.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');
$cs->registerCssFile('/assets/theme/css/separate/pages/widgets.min.css');

/*<link rel="stylesheet" href="/assets/theme/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/theme/css/main.css">*/
?>
</div>