<section class="box-typical box-typical-dashboard panel panel-default scrollable">
    <header class="box-typical-header panel-heading">
        <h3 class="panel-title"><img width="32" src="/images/interface/contract_order.png" align="middle"> Контракты поставок</h3>
    </header>
    <div class="box-typical-body panel-body">
        <table class="tbl-typical">
            <tr>
                <th><div>Товар</div></th>
                <th align="center"><div>Поставка</div></th>
                <th align="center"><div>Ходов</div></th>
                <th align="center"><div>Стоимость</div></th>
            </tr>
            <?php
            foreach ($contracts['orders'] as $contract): ?>
                <tr>
                    <td><img width="32" src="/images/items/<?=$contract->recipe->output[0]->id_item?>.png" align="middle"> <?=$contract->recipe->output[0]->item->name_item?></td>
                    <td align="center"><?=$contract->count?></td>
                    <td align="center"><?=$contract->stages?></td>
                    <td align="center"><?=$contract->price?></td>
                </tr>
            <?php endforeach;
            ?>
        </table>
    </div><!--.box-typical-body-->
</section><!--.box-typical-dashboard-->