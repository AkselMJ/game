<section class="box-typical box-typical-dashboard panel panel-default scrollable">
    <header class="box-typical-header panel-heading">
        <h3 class="panel-title"><img width="32" src="/images/interface/city.png" align="middle"> Бургерг, население <?=$city->population?></h3>
    </header>
    <div class="box-typical-body panel-body">
        <table class="tbl-typical">
            <tr>
                <th><div>Товар</div></th>
                <th align="center"><div>Запас</div></th>
                <th align="center"><div>Стоимость</div></th>
            </tr>
            <?php
            foreach ($storages as $storage): ?>
                <tr>
                    <td><img width="32" src="/images/items/<?=$storage->item->id_item?>.png" align="middle"> <?=$storage->item->name_item?></td>
                    <td align="center"><?=$storage->count?><span class="label label-danger" style="float: right">-<?=$storage->item->consume * $city->population?></span></td>
                    <td align="center"> <?=$storage->price?> </td>
                </tr>
            <?php endforeach;
            ?>
        </table>
    </div><!--.box-typical-body-->
</section><!--.box-typical-dashboard-->