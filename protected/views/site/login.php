<div class="form">
	<!-- BEGIN LOGIN FORM -->
	<?php $form = $this->beginWidget('CActiveForm', 
			array(
				'id'=>'user-login',
				'enableAjaxValidation'=>false,
				'enableClientValidation'=>false,
				'method'=>'post',
			)
	);?>

		<div class="form-group">
	
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<?php echo $form->label($model, 'username', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="1input-icon">
				<!-- i class="fa fa-user"></i-->
				<?php echo $form->textField($model, 'username', array('placeholder'=>'E-mail', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->label($model, 'password', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="1input-icon">
				<!-- i class="fa fa-lock"></i-->
				<?php echo $form->passwordField($model, 'password', array('placeholder'=>'Пароль', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix')); ?>
			</div>
		</div>
		<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message):?>
        <div id="flash-<?=$key?>" class="alert alert-danger"><?=$message?></div>
	<?php endforeach; ?>
	<?php $this->endWidget();?>
	<!-- END LOGIN FORM -->
</div>
<?php 
$script = <<<SCRIPT
document.onkeyup = function (e) {
	e = e || window.event;
	if (e.keyCode === 13) {
		$('#user-login').submit();
	}
	return true;
}
SCRIPT;
$cs=Yii::app()->getClientScript();
$cs->registerScript('loginForm',$script, CClientScript::POS_READY);
?>
