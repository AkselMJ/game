<div class="with-side-menu control-panel control-panel-compact">

    <?=$this->renderPartial("../menu-header", compact('user'), true); ?>
    <?=$this->renderPartial("../menu-side", [], true); ?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 dahsboard-column">
                    <section class="box-typical box-typical-dashboard panel panel-default scrollable">
                        <header class="box-typical-header panel-heading">
                            <h3 class="panel-title"><img width="32" src="/images/interface/contract_order.png" align="middle"> Поставки</h3>
                        </header>
                        <div class="box-typical-body panel-body">
                            <table class="tbl-typical">
                                <tr>
                                    <th><div>id_recipe</div></th>
                                    <th><div>Поставка</div></th>
                                    <th><div>Выход</div></th>
                                </tr>
                                <?php
                                foreach ($recipes[1] as $recipe): ?>
                                    <tr>
                                        <td align="center"><?=$recipe->id_recipe?></td>
                                        <td><img width="32" src="/images/items/<?=$recipe->output[0]->id_item?>.png" align="middle"> <?=$recipe->output[0]->item->name_item?></td>
                                        <td><?=$recipe->output[0]->count?></td>
                                    </tr>
                                <?php endforeach;
                                ?>
                            </table>
                        </div><!--.box-typical-body-->
                    </section><!--.box-typical-dashboard-->
                </div><!--.col-->
                <div class="col-xl-6 dahsboard-column">
                    <section class="box-typical box-typical-dashboard panel panel-default scrollable">
                        <header class="box-typical-header panel-heading">
                            <h3 class="panel-title"><img width="32" src="/images/interface/contract_order.png" align="middle"> Производство</h3>
                        </header>
                        <div class="box-typical-body panel-body">
                            <table class="tbl-typical">
                                <tr>
                                    <th><div>id_recipe</div></th>
                                    <th><div>Потребление</div></th>
                                    <th><div>Вход</div></th>
                                    <th><div>Производство</div></th>
                                    <th><div>Выход</div></th>
                                </tr>
                                <?php
                                foreach ($recipes[2] as $recipe): ?>
                                    <tr>
                                        <td align="center"><?=$recipe->id_recipe?></td>
                                        <td><img width="32" src="/images/items/<?=$recipe->input[0]->id_item?>.png" align="middle"> <?=$recipe->input[0]->item->name_item?></td>
                                        <td><?=$recipe->input[0]->count?></td>
                                        <td><img width="32" src="/images/items/<?=$recipe->output[0]->id_item?>.png" align="middle"> <?=$recipe->output[0]->item->name_item?></td>
                                        <td><?=$recipe->output[0]->count?></td>
                                    </tr>
                                <?php endforeach;
                                ?>
                            </table>
                        </div><!--.box-typical-body-->
                    </section><!--.box-typical-dashboard-->
                </div><!--.col-->
            </div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->


<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(document).ready(function() {
    $('.panel').lobiPanel({
        sortable: true,
        reload: false,
        close: false,
        editTitle: false,
        minimize: false,
        unpin: false
    });
    $('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
        $('.dahsboard-column').matchHeight();
    });  
});
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');

/*<link rel="stylesheet" href="/assets/theme/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/theme/css/main.css">*/
?>
</div>