<div class="with-side-menu control-panel control-panel-compact">

    <?=$this->renderPartial("../menu-header", compact('user'), true); ?>
    <?=$this->renderPartial("../menu-side", [], true); ?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 dahsboard-column">
                    <section class="box-typical box-typical-dashboard panel panel-default scrollable">
                        <header class="box-typical-header panel-heading">
                            <h3 class="panel-title"></h3>
                        </header>
                        <div class="box-typical-body panel-body">
                            <table class="tbl-typical">
                                <tr>
                                    <th><div>ID</div></th>
                                    <th><div>Иконка</div></th>
                                    <th><div>Товар</div></th>
                                    <th align="center"><div>Базовая цена</div></th>
                                </tr>
                                <?php
                                foreach ($items as $item): ?>
                                    <tr>
                                        <td><?=$item->id_item?></td>
                                        <td><img width="32" src="/images/items/<?=$item->id_item?>.png" align="middle"></td>
                                        <td><?=$item->name_item?></td>
                                        <td align="center"><?=$item->base_price?> </td>
                                    </tr>
                                <?php endforeach;
                                ?>
                            </table>
                        </div><!--.box-typical-body-->
                    </section><!--.box-typical-dashboard-->
                </div><!--.col-->
            </div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->


<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(document).ready(function() {
    $('.panel').lobiPanel({
        sortable: true,
        reload: false,
        close: false,
        editTitle: false,
        minimize: false,
        unpin: false
    });
    $('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
        $('.dahsboard-column').matchHeight();
    });  
});
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');

/*<link rel="stylesheet" href="/assets/theme/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/theme/css/main.css">*/
?>
</div>