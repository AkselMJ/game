<div class="with-side-menu control-panel control-panel-compact">

    <?=$this->renderPartial("../menu-header", compact('user'), true); ?>
    <?=$this->renderPartial("../menu-side", [], true); ?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 dahsboard-column">
                    <h3><i class="icon-custom icon-method" data-original-title=""></i> Методы</h3>
                    <section class="box-typical box-typical-dashboard panel panel-default scrollable">
                        <div class="element class">
                            <?php foreach ($methods as $method => $m): ?>
                                <div class="element clickable method public method_getParent_<?=$method?>" style="background-color: white;">
                                    <h2><b><?=$method?></b> - <?=$m['desc']?></h2>
                                    <pre><span class="text-primary"><?=$method?></span>(<?php
                                            $k = 0;
                                            foreach ($m['param'] as $name => $param) {
                                                echo '<span class="text-success">' . $param['type'] . '</span> ' . '<span class="text-info">$' . $name . '</span>';
                                                if (++$k != count($m['param']))
                                                    echo ", ";
                                            }
                                        ?>)</pre>
                                    <?php $file = Yii::getPathOfAlias('application') . "/runtime/returns/$method.txt";?>
                                    <?php if (file_exists($file)): ?>
                                        <div class="labels method_getParent_<?=$method?> pointer" data-toggle="collapse" data-target=".method_getParent_<?=$method?> .collapse"><a href="#">Ответ</a></div>
                                    <?php endif; ?>
                                    <div class="row collapse detail-description method_getParent_<?=$method?>">
                                        <div class="panel-body">
                                            <pre><?php if (file_exists($file)) echo file_get_contents($file); ?></pre>
                                        </div>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        </div>
                    </section>
                </div><!--.col-->
            </div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->


<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(document).ready(function() {
    $('.panel').lobiPanel({
        sortable: true,
        reload: false,
        close: false,
        editTitle: false,
        minimize: false,
        unpin: false
    });
    $('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
        $('.dahsboard-column').matchHeight();
    });  
});
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');
$cs->registerCssFile('/css/phpdocstyle.css');

/*<link rel="stylesheet" href="/assets/theme/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/theme/css/main.css">*/
?>
</div>