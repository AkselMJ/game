<div class="with-side-menu">

    <?=$this->renderPartial("../menu-header", compact('user'), true); ?>
    <?=$this->renderPartial("../menu-side", [], true); ?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <section class="box-typical box-typical-padding">
                    <form action="/?r=site/script" method="post">
                        <?php if($error): ?>
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Ошибка:</span>
                                Код не прошёл валидацию безопасности.
                            </div>
                        <?php endif; ?>
                        <textarea id="script" name="script"><?=$user->script?></textarea>
                        <br/>
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </form>
                </section>
            </div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->

<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
var editor = CodeMirror.fromTextArea(document.getElementById("script"), {
    lineNumbers: true,
    matchBrackets: true,
    mode: "application/x-httpd-php-open",
    indentUnit: 4,
    indentWithTabs: true
});
editor.setSize($('body').width() - 350, 600);
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('bootstrap');
$cs->registerPackage('codemirror');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');
?>
