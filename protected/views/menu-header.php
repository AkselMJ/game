<header class="site-header">
    <div class="container-fluid">

        <a href="#" class="site-logo">
            <img class="hidden-md-down" src="/assets/theme/img/logo-2.png" alt="">
            <img class="hidden-lg-up" src="/assets/theme/img/logo-2-mob.png" alt="">
        </a>

        <button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
            <span>toggle menu</span>
        </button>

        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    <div class="dropdown dropdown-notification">
                        <div class="amount color-blue" style="margin-top: 5px;"><?=number_format($user->money, 0, ',', ' ')?></div>
                    </div>
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?=Yii::app()->user->name?>
                            <img src="/assets/theme/img/avatar-2-64.png" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <?php /*<a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                            <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
                            <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-question-sign"></span>Help</a>
                            <div class="dropdown-divider"></div>*/?>
                            <?php  if (Yii::app()->user->isGuest): ?>
                                <a class="dropdown-item" href="/?r=user/login"><span class="font-icon glyphicon glyphicon-log-out"></span>Войти</a>
                                <a class="dropdown-item" href="/?r=user/register"><span class="font-icon glyphicon glyphicon-log-out"></span>Регистрация</a>
                            <?php else: ?>
                                <a class="dropdown-item" href="/?r=user/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Выйти</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->