<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <form class="sign-box" action="/?r=user/login" method="post">
                <div class="sign-avatar">
                    <img src="/assets/theme/img/avatar-sign.png" alt="">
                </div>
                <header class="sign-title">Вход в игру</header>
                <div class="form-group">
                    <input type="text" name="LoginForm[username]" class="form-control" placeholder="Логин"/>
                </div>
                <div class="form-group">
                    <input type="password" name="LoginForm[password]" class="form-control" placeholder="Пароль"/>
                </div>
                <div class="form-group">
                    <?php /*<div class="checkbox float-left">
                        <input type="checkbox" id="signed-in"/>
                        <label for="signed-in">Keep me signed in</label>
                    </div>*/ ?>
                    <?php /*<div class="float-right reset">
                        <a href="reset-password.html">Reset Password</a>
                    </div>*/ ?>
                </div>
                <button type="submit" class="btn btn-rounded">Войти</button>
                <p class="sign-note">Нет аккаунта? <a href="/?r=user/register">Регистрация</a></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->

<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(function() {
    $('.page-center').matchHeight({
        target: $('html')
    });

    $(window).resize(function(){
        setTimeout(function(){
            $('.page-center').matchHeight({ remove: true });
            $('.page-center').matchHeight({
                target: $('html')
            });
        },100);
    });
});
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');
$cs->registerCssFile('/assets/theme/css/separate/pages/login.min.css');
?>