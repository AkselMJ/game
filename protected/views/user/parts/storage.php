<?php if (!Yii::app()->user->isGuest): ?>
<section class="box-typical box-typical-dashboard panel panel-default scrollable">
    <header class="box-typical-header panel-heading">
        <h3 class="panel-title"><img width="32" src="/images/interface/storage_user.png" align="middle"> Склад</h3>
    </header>
    <div class="box-typical-body panel-body">
        <table class="tbl-typical">
            <tr>
                <th><div>Товар</div></th>
                <th align="center"><div>Запас</div></th>
            </tr>
            <?php
            foreach ($user_storage as $storage): ?>
                <tr>
                    <td><img width="32" src="/images/items/<?=$storage->item->id_item?>.png" align="middle"> <?=$storage->item->name_item?></td>
                    <td align="center"><?=$storage->count?></td>
                </tr>
            <?php endforeach;
            ?>
        </table>
    </div><!--.box-typical-body-->
</section><!--.box-typical-dashboard-->
<?php endif; ?>