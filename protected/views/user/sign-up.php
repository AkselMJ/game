<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <form class="sign-box" action="/?r=user/register" method="post">
                <div class="sign-avatar no-photo">&plus;</div>
                <header class="sign-title">Регистрация аккаунта</header>
                <div class="form-group">
                    <input type="text" name="LoginForm[username]" class="form-control" placeholder="Логин"/>
                </div>
                <div class="form-group">
                    <input type="password" name="LoginForm[password]" class="form-control" placeholder="Пароль"/>
                </div>
                <div class="form-group">
                    <input type="password" name="LoginForm[repeat]" class="form-control" placeholder="Повтор пароля"/>
                </div>
                <button type="submit" class="btn btn-rounded btn-success sign-up">Регистрация</button>
                <p class="sign-note">Зарегистрированы? <a href="/?r=user/login">Войти</a></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->

<?php
$cs = Yii::app()->getClientScript();

$script = <<<SCRIPT
$(function() {
    $('.page-center').matchHeight({
        target: $('html')
    });

    $(window).resize(function(){
        setTimeout(function(){
            $('.page-center').matchHeight({ remove: true });
            $('.page-center').matchHeight({
                target: $('html')
            });
        },100);
    });
});
SCRIPT;

$cs->registerScript('main', $script, CClientScript::POS_READY);
$cs->registerPackage('jquery');
$cs->registerPackage('jqueryui');
$cs->registerPackage('bootstrap');
$cs->registerPackage('lobipanel');

$cs->registerScriptFile('/assets/theme/js/plugins.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/lib/match-height/jquery.matchHeight.min.js', CClientScript::POS_END);
$cs->registerScriptFile('/assets/theme/js/app.js', CClientScript::POS_END);

$cs->registerCssFile('/assets/theme/css/main.css');
$cs->registerCssFile('/assets/theme/css/separate/pages/login.min.css');
?>