<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="blue">
            <a href="/?r=site/index">
	            <span>
	                <i class="font-icon font-icon-dashboard"></i>
	                <span class="lbl">Главная</span>
	            </span>
            </a>
        </li>
        <li class="brown">
            <a href="/?r=site/script">
	            <span>
	                <i class="font-icon font-icon-notebook"></i>
	                <span class="lbl">Скрипт</span>
	            </span>
            </a>
        </li>
        <li class="green">
            <a href="/?r=site/api">
	            <span>
	                <i class="font-icon font-icon-widget"></i>
	                <span class="lbl">API</span>
	            </span>
            </a>
        </li>
        <li class="green">
            <a href="/?r=site/items">
	            <span>
	                <i class="glyphicon glyphicon-th"></i>
	                <span class="lbl">Предметы</span>
	            </span>
            </a>
        </li>
        <li class="magenta">
            <a href="/?r=site/recipes">
	            <span>
	                <i class="glyphicon glyphicon-cog"></i>
	                <span class="lbl">Схемы</span>
	            </span>
            </a>
        </li>
    </ul>

</nav><!--.side-menu-->