<?php

defined('YII_DEBUG') or define('YII_DEBUG',true);

error_reporting(E_ALL);

// include Yii bootstrap file
$yii = dirname(__FILE__) . '/framework/yii.php';

// create a Web application instance and run
$config = dirname(__FILE__) . '/protected/config/config.php';


require_once($yii);
Yii::createWebApplication($config)->run();